package org.example.poi;

import com.documents4j.api.DocumentType;
import com.documents4j.api.IConverter;
import com.documents4j.job.LocalConverter;
import org.apache.poi.xwpf.usermodel.*;
import org.apache.xmlbeans.XmlCursor;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTSdtBlock;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTSdtCell;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTSdtRun;
import org.w3c.dom.Node;

import javax.xml.namespace.QName;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

public class Application {

    public static void main(String[] args) throws IOException {
        FileInputStream in = new FileInputStream("oui.docx");
        XWPFDocument document = new XWPFDocument(in);

        modify(document, document.getBodyElements());

        FileOutputStream out = new FileOutputStream("buffer.docx");
        document.write(out);

        document.close();
        out.close();
        in.close();

        convert();

        new File("buffer.docx").delete();
    }

    private static void convert() throws IOException {
        FileInputStream input = new FileInputStream("buffer.docx");
        FileOutputStream output = new FileOutputStream("bonjour.pdf");
        IConverter converter = LocalConverter.builder().build();

        converter.convert(input).as(DocumentType.MS_WORD)
                .to(output).as(DocumentType.PDF)
                .execute();

        converter.shutDown();

        input.close();
        output.close();
    }

    private static void modify(XWPFDocument document, List<IBodyElement> bodyElements) {
        for (IBodyElement element : bodyElements) {
            if (element instanceof XWPFParagraph) {
                XWPFParagraph paragraph = (XWPFParagraph) element;
                paragraphRender(document, paragraph);

            } else if (element instanceof XWPFTable) {
                XWPFTable table = (XWPFTable) element;

                for (XWPFTableRow row : table.getRows()) {
                    for (XWPFTableCell cell : row.getTableCells()) {
                        modify(document, cell.getBodyElements());
                    }
                }
            }
        }
    }

    private static void paragraphRender(XWPFDocument document, XWPFParagraph paragraph) {
        XmlCursor cursor = paragraph.getCTP().newCursor();

        while (cursor.hasNextToken()) {
            XmlCursor.TokenType token = cursor.toNextToken();

            if (token.isStart()) {
                XWPFAbstractSDT sdt = null;

                if (cursor.getObject() instanceof CTSdtRun) {
                    sdt = new XWPFSDT((CTSdtRun) cursor.getObject(), document);
                } else if (cursor.getObject() instanceof CTSdtBlock) {
                    sdt = new XWPFSDT((CTSdtBlock) cursor.getObject(), document);
                } else if (cursor.getObject() instanceof CTSdtCell) {
                    sdt = new XWPFSDTCell((CTSdtCell) cursor.getObject(), null, null);
                }

                if (sdt != null && !sdt.getTitle().isEmpty()) {
                    System.out.println("-------------------");
                    System.out.println(cursor.getObject());

                    Node domNode = cursor.getObject().getDomNode();
                    for (int i=0; i<domNode.getChildNodes().getLength(); i++) {
                        if (domNode.getChildNodes().item(i).getNodeName().equals("w:sdtContent")) {
                            Node contentNode = domNode.getChildNodes().item(i);
                            int loopStart = 0;
                            if (!contentNode.getChildNodes().item(0).getNodeName().equals("w:r")) {
                                contentNode = contentNode
                                        .getChildNodes().item(0)    // w:tc
                                        .getChildNodes().item(1);   // w:p
                                loopStart = 1;
                            }

                            boolean done = false;
                            for (int j=loopStart; j<contentNode.getChildNodes().getLength(); j++) {
                                Node value = contentNode
                                        .getChildNodes().item(j)          // w:r
                                        .getChildNodes().item(1);    // w:t

                                if (value.getNodeName().equals("w:t")) {
                                    if (done) {
                                        value.getChildNodes().item(0).setNodeValue("");
                                    } else {
                                        value.getChildNodes().item(0).setNodeValue("oui");
                                        done = true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
